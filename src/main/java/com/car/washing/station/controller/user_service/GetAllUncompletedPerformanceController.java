package com.car.washing.station.controller.user_service;

import com.car.washing.station.entity.PerformanceOfServices;
import com.car.washing.station.repository.performance.PerformanceRepository;
import com.car.washing.station.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/performance/all")
public class GetAllUncompletedPerformanceController {
    @Autowired
    private PerformanceRepository performanceRepository;
    @Autowired
    private UserRepository userRepository;

    @GetMapping(path = "{userId}")
    public ResponseEntity<List<PerformanceOfServices>> getAllUncompletedPerformance(
            @PathVariable int userId
    ) {
        if (userRepository.findById(userId) == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(performanceRepository.findByUserAndIsComplete(userId, 0));
    }
}
