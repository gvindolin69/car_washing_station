package com.car.washing.station.modules;

import com.car.washing.station.entity.Service;
import lombok.AllArgsConstructor;
import com.car.washing.station.entity.PerformanceOfServices;
import com.car.washing.station.repository.performance.PerformanceRepository;

import java.text.SimpleDateFormat;
import java.util.*;

@AllArgsConstructor
public class EmptySpacesForServiceCreator {
    private final PerformanceRepository performanceRepository;
    private final CurrentTimeDoteCreator currentTimeDoteCreater;

    public List<List<Integer>> createEmptySpacesForService(Service service, String date) {
        List<List<Integer>> listOfEmptySpacesForService = new ArrayList<>();
        List<Interval> listOfEmptySpaces = createListOfEmptySpaces(createTimeDotes(date), date);

        for (Interval emptySpace : listOfEmptySpaces) {
            if (emptySpace.to - emptySpace.from >= service.getLonging()) {
                listOfEmptySpacesForService.add(Arrays.asList(emptySpace.from, emptySpace.to));
            }
        }

        return listOfEmptySpacesForService;
    }

    private List<Interval> createListOfEmptySpaces(List<Interval> timeDotes, String date) {
        int startTimeDote = 0;
        int endTimeDote = 1440;
        List<Interval> listOfEmptySpaces = new ArrayList<>();

        listOfEmptySpaces.add(new Interval((timeDotes.get(timeDotes.size()-1).to), endTimeDote));

        if (date.equals(createDate()) && currentTimeDoteCreater.createCurrentTimeDote() < timeDotes.get(0).from){
            listOfEmptySpaces.add(new Interval(currentTimeDoteCreater.createCurrentTimeDote(), timeDotes.get(0).from));
        }
        if (!date.equals(createDate())){
            listOfEmptySpaces.add(new Interval(startTimeDote, timeDotes.get(0).from));
        }

        for (int i = 0; i < timeDotes.size() - 1; i++) {
            listOfEmptySpaces.add(new Interval(timeDotes.get(i).to, timeDotes.get(i + 1).from));
        }

        return listOfEmptySpaces;
    }

    private List<Interval> createTimeDotes(String date) {
        List<PerformanceOfServices> performanceOfServicesList = performanceRepository.findByDateAndIsComplete(date, 0);
        List<Interval> timeDotes = new ArrayList<>();
        for (PerformanceOfServices performance : performanceOfServicesList) {
            timeDotes.add(new Interval(performance.getTimeFrom(), performance.getTimeTo()));
        }
        timeDotes.sort(Comparator.comparingInt(interval -> interval.from));

        return timeDotes;
    }

    private String createDate() {
        Date date = Calendar.getInstance().getTime();
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }


    static class Interval{
        public int from;
        public int to;

        public Interval(int from, int to) {
            this.from = from;
            this.to = to;
        }
    }
}
