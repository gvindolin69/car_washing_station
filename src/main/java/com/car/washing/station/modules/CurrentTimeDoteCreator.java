package com.car.washing.station.modules;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class CurrentTimeDoteCreator {
    public int createCurrentTimeDote() {
        LocalTime time = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        String currentTime = (time.format(formatter));
        String[] splitedTime = (currentTime.split(":"));
        return Integer.parseInt(splitedTime[0]) * 60 + Integer.parseInt(splitedTime[1]);
    }
}
