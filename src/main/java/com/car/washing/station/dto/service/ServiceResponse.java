package com.car.washing.station.dto.service;

import lombok.Data;

@Data
public class ServiceResponse {
    private int id;
    private String title;
    private int cost;
    private long longing;
}
