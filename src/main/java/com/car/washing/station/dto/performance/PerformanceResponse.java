package com.car.washing.station.dto.performance;

import lombok.Data;

@Data
public class PerformanceResponse {
    private int id;
    private String serviceTitle;
    private int userId;
    private String date;
    private int timeFrom;
    private int timeTo;
    private int isCompleted;
}
