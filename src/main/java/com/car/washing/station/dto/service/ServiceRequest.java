package com.car.washing.station.dto.service;

import lombok.Data;

@Data
public class ServiceRequest {
    private String title;
    private int cost;
    private int longing;
}