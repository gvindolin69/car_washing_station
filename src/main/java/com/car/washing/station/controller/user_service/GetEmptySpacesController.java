package com.car.washing.station.controller.user_service;

import com.car.washing.station.entity.Service;
import com.car.washing.station.modules.EmptySpacesForServiceCreator;
import com.car.washing.station.repository.service.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/performance/empty-spaces")
public class GetEmptySpacesController {
    @Autowired
    private EmptySpacesForServiceCreator creator;
    @Autowired
    private ServiceRepository serviceRepository;

    @GetMapping
    public ResponseEntity<List<List<Integer>>> getEmptySpacesForDateAndService(
            @RequestParam String date,
            @RequestParam String serviceTitle
    ) {
        Service service = serviceRepository.findByTitle(serviceTitle);
        return ResponseEntity.ok(creator.createEmptySpacesForService(service, date));
    }
}
