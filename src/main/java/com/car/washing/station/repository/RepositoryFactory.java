package com.car.washing.station.repository;

import com.car.washing.station.repository.performance.PerformanceRepository;
import com.car.washing.station.repository.service.ServiceRepository;
import com.car.washing.station.repository.user.UserRepository;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class RepositoryFactory {
    @Bean
    public DataSource dataSource(
            @Value("${javax.persistence.jdbc.url}") String url,
            @Value("${javax.persistence.jdbc.driver}") String driver,
            @Value("${javax.persistence.jdbc.user}") String username,
            @Value("${javax.persistence.jdbc.password}") String password
    ) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean("washing.station.entity.manager.factory")
    public EntityManagerFactory createEntityManagerFactory(
            DataSource dataSource,
            @Value("${hibernate.dialect}") String dialect,
            @Value("${hibernate.show_sql}") String showSql,
            @Value("${format_sql}") String formatSql,
            @Value("${hibernate.connection.charSet}") String charSet,
            @Value("${hibernate.hbm2ddl.auto}") String hbm2ddl
    ) {
        LocalContainerEntityManagerFactoryBean localContainer = new LocalContainerEntityManagerFactoryBean();
        Properties properties = new Properties();

        properties.put("hibernate.dialect", dialect);
        properties.put("hibernate.show_sql", showSql);
        properties.put("format_sql", formatSql);
        properties.put("hibernate.connection.charSet", charSet);
        properties.put("hibernate.hbm2ddl.auto", hbm2ddl);

        localContainer.setDataSource(dataSource);
        localContainer.setJpaProperties(properties);
        localContainer.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        localContainer.setPackagesToScan("com.car.washing.station.entity");
        localContainer.setPersistenceUnitName("cws.unit");
        localContainer.afterPropertiesSet();

        return localContainer.getObject();
    }

    @Bean("user.repository")
    public UserRepository createUserRepository(
            @Qualifier("create.entity.manager.user")
            EntityManager entityManager
    ) {
        return new UserRepository(entityManager);
    }

    @Bean("service.repository")
    public ServiceRepository createServiceRepository(
            @Qualifier("create.entity.manager.service") EntityManager entityManager
    ) {
        return new ServiceRepository(entityManager);
    }

    @Bean("performance.repository")
    public PerformanceRepository createPerformanceRepository(
            @Qualifier("create.entity.manager.performance") EntityManager entityManager
    ) {
        return new PerformanceRepository(entityManager);
    }

    @Bean("create.entity.manager.performance")
    public EntityManager createEntityManagerForPerformanceRepository(

            EntityManagerFactory entityManagerFactory
    ) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean("create.entity.manager.user")
    public EntityManager createEntityManagerForUserRepository(

            EntityManagerFactory entityManagerFactory
    ) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean("create.entity.manager.service")
    public EntityManager createEntityManagerForServiceRepository(

            EntityManagerFactory entityManagerFactory
    ) {
        return entityManagerFactory.createEntityManager();
    }
}

