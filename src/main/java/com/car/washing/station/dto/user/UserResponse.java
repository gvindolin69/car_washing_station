package com.car.washing.station.dto.user;

import lombok.Data;

@Data
public class UserResponse {
    private int id;
    private String email;
    private String fullName;
    private String roles;
}
