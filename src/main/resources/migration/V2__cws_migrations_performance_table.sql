CREATE TABLE "performance" (
	"id" SERIAL NOT NULL,
	"service_title"	varchar(255),
	"user_id" INTEGER REFERENCES "user" (id),
	"date" varchar(255),
	"time_from" int4,
	"time_to" int4,
	"is_completed" int4,
	PRIMARY KEY("id")
);