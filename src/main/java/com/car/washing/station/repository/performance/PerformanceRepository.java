package com.car.washing.station.repository.performance;

import com.car.washing.station.entity.PerformanceOfServices;
import lombok.AllArgsConstructor;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@AllArgsConstructor
public class PerformanceRepository {
    private final EntityManager entityManager;

    public List<PerformanceOfServices> findByDateAndIsComplete(String date, int isCompleted) {
        return (List<PerformanceOfServices>) entityManager
                .createQuery("SELECT p FROM PerformanceOfServices p WHERE p.isCompleted = :isCompleted AND p.date = :date")
                .setParameter("isCompleted", isCompleted)
                .setParameter("date", date)
                .getResultList();
    }

    public PerformanceOfServices findById(int id) {
        try {
            return (PerformanceOfServices) entityManager.createQuery("SELECT p FROM PerformanceOfServices p WHERE p.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    public List<PerformanceOfServices> findByUserAndIsComplete(int userId, int isCompleted) {
        return (List<PerformanceOfServices>) entityManager
                .createQuery("SELECT p FROM PerformanceOfServices p WHERE p.isCompleted = :isCompleted AND p.userId = :userId")
                .setParameter("isCompleted", isCompleted)
                .setParameter("userId", userId)
                .getResultList();
    }

    public void save(PerformanceOfServices performance) {
        entityManager.getTransaction().begin();
        entityManager.persist(performance);
        entityManager.getTransaction().commit();
    }

    public void saveList(List<PerformanceOfServices> darazData) {
        entityManager.getTransaction().begin();
        for (PerformanceOfServices data : darazData) {
            entityManager.persist(data);
        }
        entityManager.getTransaction().commit();
    }
}
