# car_washing_station

Перед запуском приложения необходимо поднять докер,
``````
docker-compose up -d --build
``````
создать переменную окружения {APP_DATABASE_CONNECTION_URL} -
которая является путем к базе данных.

Которая выглядит таким образом:

``````
jdbc:postgresql://localhost:2020/car_washing_station
``````

И произвести миграции

````
./gradlew clean build -PappDataBaseConnectionUrl="$APP_DATABASE_CONNECTION_URL" -xtest flywayMigrate
````

Swagger_port:8080, server_port:8091