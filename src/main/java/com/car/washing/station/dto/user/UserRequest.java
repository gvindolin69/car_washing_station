package com.car.washing.station.dto.user;

import lombok.Data;

@Data
public class UserRequest {
    private String email;
    private String fullName;
    private String password;
    private String roles;
}
