package com.car.washing.station.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@Configuration
public class ScheduledConfiguration {
    private final Runnable performanceUpdater;

    public ScheduledConfiguration(@Qualifier("performance.updater") Runnable performanceUpdater) {
        this.performanceUpdater = performanceUpdater;
    }

    @Scheduled(fixedRate = 30000)
    public void updatePerformanceOfServices() {
        performanceUpdater.run();
    }
}
