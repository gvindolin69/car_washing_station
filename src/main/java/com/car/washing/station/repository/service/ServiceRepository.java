package com.car.washing.station.repository.service;

import com.car.washing.station.entity.Service;
import lombok.AllArgsConstructor;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@AllArgsConstructor
public class ServiceRepository {
    private final EntityManager entityManager;

    public List<Service> getList() {
        return (List<Service>) entityManager.createQuery("SELECT s FROM Service s").getResultList();
    }

    public void save(Service service) {
        entityManager.getTransaction().begin();
        entityManager.persist(service);
        entityManager.getTransaction().commit();
    }

    public Service findByTitle(String title) {
        try {
            return (Service) entityManager.createQuery("SELECT s FROM Service s WHERE s.title = :title")
                    .setParameter("title", title)
                    .getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    public Service findById(int id) {
        try {
            return (Service) entityManager.createQuery("SELECT s FROM Service s WHERE s.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    public void delete(int id) {
        Service service = entityManager.find(Service.class, id);
        entityManager.getTransaction().begin();
        entityManager.remove(service);
        entityManager.getTransaction().commit();
    }
}