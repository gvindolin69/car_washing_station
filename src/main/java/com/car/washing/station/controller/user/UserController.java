package com.car.washing.station.controller.user;

import com.car.washing.station.entity.User;
import com.car.washing.station.repository.user.UserRepository;
import com.car.washing.station.dto.user.UserRequest;
import com.car.washing.station.dto.user.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<?> createUser(@RequestBody UserRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if(userRepository.findByEmail(request.getEmail()) == null){
            User user = createUserEntity(request);
            userRepository.save(user);
            return ResponseEntity.ok(createUserResponse(user));
        } else if (
                auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER"))&&
                request.getRoles().equals("ROLE_ADMINISTRATOR")
        ) {
            return new ResponseEntity<>("Forbidden", HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>("User with this email already exists", HttpStatus.CONFLICT);
    }

    @PutMapping(path = "{userId}", consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<?> updateUser(
            @PathVariable int userId,
            @RequestBody UserRequest request
    ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (userRepository.findById(userId) == null) {
            return ResponseEntity.notFound().build();
        } else if (
            auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER"))&&
                    request.getRoles().equals("ROLE_ADMINISTRATOR")
        ) {
            return new ResponseEntity<>("Forbidden", HttpStatus.FORBIDDEN);
        }

        User user = createUserEntity(request);
        userRepository.save(user);
        return ResponseEntity.ok(createUserResponse(user));
    }

    @DeleteMapping(path = "{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable int userId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMINISTRATOR"))) {
            if (userRepository.findById(userId) == null) {
                return ResponseEntity.notFound().build();
            }

            userRepository.delete(userId);

            return ResponseEntity.ok("User with id " + userId + " was deleted");
        }
        return new ResponseEntity<>("Forbidden", HttpStatus.FORBIDDEN);
    }

    private UserResponse createUserResponse(User user) {
        UserResponse response = new UserResponse();
        response.setEmail(user.getEmail());
        response.setRoles(user.getRole());
        response.setFullName(user.getFullName());
        response.setId(user.getId());

        return response;
    }

    private User createUserEntity(UserRequest request) {
        User user = new User();
        user.setRole(request.getRoles());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setFullName(request.getFullName());
        user.setEmail(request.getEmail());
        return user;
    }
}
