package com.car.washing.station.modules;

import lombok.AllArgsConstructor;
import com.car.washing.station.entity.PerformanceOfServices;
import com.car.washing.station.repository.performance.PerformanceRepository;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

@AllArgsConstructor
public class PerformanceOfServicesUpdater implements Runnable {
    private final PerformanceRepository performanceRepository;
    private final CurrentTimeDoteCreator currentTimeDoteCreater;

    @Override
    public void run() {
        performanceRepository.saveList(
                performanceRepository.findByDateAndIsComplete(createDate(), 0)
                        .stream()
                        .peek((performance) -> {
                            if (isCompletedChecker(performance)) {
                                performance.setIsCompleted(1);
                            }
                        })
                        .filter((performance) -> performance.getIsCompleted() == 1)
                        .collect(Collectors.toList())
        );
    }

    private String createDate() {
        Date date = Calendar.getInstance().getTime();
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    private boolean isCompletedChecker(PerformanceOfServices performance) {
        return performance.getTimeFrom() <= currentTimeDoteCreater.createCurrentTimeDote();
    }
}
