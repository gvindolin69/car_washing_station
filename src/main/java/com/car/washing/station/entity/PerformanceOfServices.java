package com.car.washing.station.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Table(name = "`performance`")
public class PerformanceOfServices {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;
    @Column(name = "service_title")
    private String serviceTitle;
    @Column(name = "user_id")
    private int userId;
    @Column(name = "date")
    private String date;
    @Column(name = "time_from")
    private int timeFrom;
    @Column(name = "time_to")
    private int timeTo;
    @Column(name = "is_completed")
    private int isCompleted;

    public PerformanceOfServices() {
    }
}
