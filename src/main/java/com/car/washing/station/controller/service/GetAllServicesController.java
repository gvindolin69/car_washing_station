package com.car.washing.station.controller.service;

import com.car.washing.station.dto.service.ServiceResponse;
import com.car.washing.station.entity.Service;
import com.car.washing.station.repository.service.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/service/all")
public class GetAllServicesController {
    @Autowired
    private ServiceRepository serviceRepository;

    @GetMapping
    public ResponseEntity<?> getListOfUsers(){
        List<Service> serviceList = serviceRepository.getList();
        List<ServiceResponse> responseList = new ArrayList<>();
        for (Service service : serviceList) {
            responseList.add(createServiceResponse(service));
        }
        return ResponseEntity.ok(responseList);
    }

    private ServiceResponse createServiceResponse(Service service) {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setCost(service.getCost());
        serviceResponse.setTitle(service.getTitle());
        serviceResponse.setLonging(service.getLonging());
        serviceResponse.setId(service.getId());
        return serviceResponse;
    }
}
