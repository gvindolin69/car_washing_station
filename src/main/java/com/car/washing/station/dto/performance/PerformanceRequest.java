package com.car.washing.station.dto.performance;

import lombok.Data;

@Data
public class PerformanceRequest {
    private String serviceTitle;
    private int userId;
    private String date;
    private int timeFrom;
    private int timeTo;
    private int isCompleted;
}
