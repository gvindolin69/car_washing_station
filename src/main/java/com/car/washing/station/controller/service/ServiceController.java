package com.car.washing.station.controller.service;

import com.car.washing.station.entity.Service;
import com.car.washing.station.repository.service.ServiceRepository;
import com.car.washing.station.dto.service.ServiceRequest;
import com.car.washing.station.dto.service.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/service")
public class ServiceController {
    @Autowired
    private ServiceRepository serviceRepository;

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<?> createService(@RequestBody ServiceRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMINISTRATOR"))) {
            Service service = createServiceEntity(request);

            serviceRepository.save(service);

            return ResponseEntity.ok(createServiceResponse(service));
        }
        return new ResponseEntity<>("Unauthorized", HttpStatus.FORBIDDEN);
    }

    @PutMapping(path = "{serviceId}", consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<?> updateService(
            @PathVariable int serviceId,
            @RequestBody ServiceRequest request
    ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMINISTRATOR"))) {
            if (serviceRepository.findById(serviceId) == null) {
                return ResponseEntity.notFound().build();
            }
            Service service = createServiceEntity(request);

            serviceRepository.save(service);

            return ResponseEntity.ok(createServiceResponse(service));
        }
        return new ResponseEntity<>("Unauthorized", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping(path = "{serviceId}")
    public ResponseEntity<String> deleteService(@PathVariable int serviceId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMINISTRATOR"))) {
            if (serviceRepository.findById(serviceId) == null) {
                return ResponseEntity.notFound().build();
            }

            serviceRepository.delete(serviceId);

            return ResponseEntity.ok("Service with id " + serviceId + " was deleted");
        }
        return new ResponseEntity<>("Unauthorized", HttpStatus.FORBIDDEN);
    }

    private ServiceResponse createServiceResponse(Service service) {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setCost(service.getCost());
        serviceResponse.setTitle(service.getTitle());
        serviceResponse.setLonging(service.getLonging());
        serviceResponse.setId(service.getId());
        return serviceResponse;
    }

    private Service createServiceEntity(ServiceRequest request) {
        Service service = new Service();
        service.setTitle(request.getTitle());
        service.setCost(request.getCost());
        service.setLonging(request.getLonging());
        return service;
    }
}
