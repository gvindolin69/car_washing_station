package com.car.washing.station.repository.user;

import com.car.washing.station.entity.User;
import lombok.AllArgsConstructor;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

@AllArgsConstructor
public class UserRepository {
    private final EntityManager entityManager;

    public void save(User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    public User findByEmail(String email) {
        try {
            return (User) entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email")
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    public User findById(int id) {
        try {
            return (User) entityManager.createQuery("SELECT u FROM User u WHERE u.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    public void delete(int id) {
        User user = entityManager.find(User.class, id);
        entityManager.getTransaction().begin();
        entityManager.remove(user);
        entityManager.getTransaction().commit();
    }

    private void update(User user) {
        User oldVersion = entityManager.find(User.class, user.getId());
        entityManager.getTransaction().begin();
        oldVersion.setEmail(user.getEmail());
        oldVersion.setFullName(user.getFullName());
        oldVersion.setPassword(user.getPassword());
        oldVersion.setRole(user.getRole());
        entityManager.getTransaction().commit();
    }
}
