#!/bin/bash

create_database_ () {
psql --username=postgres <<-EOSQL
CREATE USER $1 WITH PASSWORD '$1';
CREATE DATABASE $1 WITH OWNER $1;
GRANT ALL PRIVILEGES ON DATABASE $1 TO $1;
EOSQL
}

### car_washing_station ###
create_database_ car_washing_station
