package com.car.washing.station.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class Md5PasswordEncoder implements PasswordEncoder {

    private final Logger logger = LoggerFactory.getLogger(Md5PasswordEncoder.class);

    @Override
    public String encode(CharSequence rawPassword) {
        try {
            byte[] bytesOfPassword = rawPassword.toString().getBytes(StandardCharsets.UTF_8);
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(bytesOfPassword);
            byte[] encodedPassword = messageDigest.digest();
            return DatatypeConverter.printHexBinary(encodedPassword);
        } catch (NoSuchAlgorithmException exception) {
            return null;
        }
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        if (rawPassword == null) {
            throw new IllegalArgumentException("rawPassword cannot be null");
        }
        if (encodedPassword == null || encodedPassword.length() == 0) {
            this.logger.warn("Empty encoded password");
            return false;
        }
        return encode(rawPassword).equals(encodedPassword);
    }
}
