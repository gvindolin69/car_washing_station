package com.car.washing.station.configuration;

import com.car.washing.station.modules.CurrentTimeDoteCreator;
import com.car.washing.station.modules.EmptySpacesForServiceCreator;
import com.car.washing.station.modules.PerformanceOfServicesUpdater;
import com.car.washing.station.repository.performance.PerformanceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectConfiguration {
    @Bean("performance.updater")
    public Runnable createPerformanceUpdater(PerformanceRepository repository, CurrentTimeDoteCreator currentTimeDoteCreater) {
        return new PerformanceOfServicesUpdater(repository, currentTimeDoteCreater);
    }

    @Bean
    public CurrentTimeDoteCreator createCreater() {
        return new CurrentTimeDoteCreator();
    }

    @Bean
    public EmptySpacesForServiceCreator createEmptySpacesCreater(
            PerformanceRepository performanceRepository,
            CurrentTimeDoteCreator currentTimeDoteCreater
    ) {
        return new EmptySpacesForServiceCreator(performanceRepository, currentTimeDoteCreater);
    }
}
