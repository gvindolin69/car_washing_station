package com.car.washing.station.controller.user_service;

import com.car.washing.station.entity.PerformanceOfServices;
import com.car.washing.station.modules.CurrentTimeDoteCreator;
import com.car.washing.station.repository.performance.PerformanceRepository;
import com.car.washing.station.dto.performance.PerformanceRequest;
import com.car.washing.station.dto.performance.PerformanceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/performance")
public class PerformanceController {
    @Autowired
    private PerformanceRepository performanceRepository;
    @Autowired
    private CurrentTimeDoteCreator currentTimeDoteCreator;

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<PerformanceResponse> createPerformance(@RequestBody PerformanceRequest request) {
        PerformanceOfServices performance = createPerformanceEntity(request);

        performanceRepository.save(performance);

        return ResponseEntity.ok(createPerformanceResponse(performance));

    }

    @GetMapping(path = "{performanceId}")
    public ResponseEntity<Integer> getTimeRemainingToService(
            @PathVariable int performanceId
    ) {
        if (performanceRepository.findById(performanceId) == null) {
            return ResponseEntity.notFound().build();
        }

        PerformanceOfServices performance = performanceRepository.findById(performanceId);

        return ResponseEntity.ok(performance.getTimeFrom() - currentTimeDoteCreator.createCurrentTimeDote());
    }

    private PerformanceResponse createPerformanceResponse(PerformanceOfServices performance) {
        PerformanceResponse performanceResponse = new PerformanceResponse();
        performanceResponse.setId(performance.getId());
        performanceResponse.setDate(performance.getDate());
        performanceResponse.setUserId(performance.getUserId());
        performanceResponse.setTimeFrom(performance.getTimeFrom());
        performanceResponse.setTimeTo(performance.getTimeTo());
        performanceResponse.setServiceTitle(performance.getServiceTitle());
        return performanceResponse;
    }

    private PerformanceOfServices createPerformanceEntity(PerformanceRequest request) {
        PerformanceOfServices performance = new PerformanceOfServices();
        performance.setDate(request.getDate());
        performance.setUserId(request.getUserId());
        performance.setServiceTitle(request.getServiceTitle());
        performance.setTimeFrom(request.getTimeFrom());
        performance.setTimeTo(request.getTimeTo());
        return performance;
    }
}
